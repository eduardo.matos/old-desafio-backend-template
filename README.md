# Desafio Code Review Pagar.me (v1.0)

Em programação investimos mais tempo lendo código (para review, entender o que está sendo feita, argumentar, etc) do que escrevendo.

No Pagar.me, é comum ter diversos times e pessoas colocando código em produção constantemente e uma coisa que está no nosso dia-a-dia é Code Review.

Nesse desafio, queremos que você faça o Code Review de uma implementação que alguma outra pessoa do time quer colocar em produção. Para você ter uma experiência o mais parecida possível com o nosso dia-a-dia, criamos esse repositório vazio com um Pull Request aberto para você revisar através da interface do Github.

> Importante: Leia com atenção o Contexto, os Requisitos e o Formato de Avaliação

## Instruções

1. Leia com atenção o Contexto, os Requisitos e o Formato de Avaliação
1. Faça o review através do "Pull Request" que se encontra nesse repositório. O processo é bem intuitivo, mas caso tenha dúvidas de como fazer isso, você pode consultar o link: https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/about-pull-request-reviews
    > **Importante:** garanta que o seu pull request não esteja "pending" quando for nos enviar o desafio. [Veja esse link](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/reviewing-proposed-changes-in-a-pull-request#submitting-your-review) caso tenha dúvidas
1. Para fazer um bom desafio, considere que esse código será colocado em produção e impactar milhares de clientes. Coloque essa importância no seu review. Imagine que a responsabilidade por esse código estar em produção é 100% sua. 
1. Considere que você está fazendo o review do código de uma pessoa do seu time e se comunique como você se comunicaria nessa situação.
1. Você pode sugerir soluções para os problemas que você encontrar nos comentários do review.

## Contexto

Em sua essência um PSP tem duas funções muito importantes:

1. Permitir que nossos clientes processem transações ("cash-in")
2. Efetuar os pagamentos dos recebíveis para os nossos clientes ("cash-out")

No Pagar.me, nós temos duas entidades que representam essas informações:

* `transactions`: que representam as informações da compra, dados do cartão, valor, etc
* `payables`: que representam os recebíveis que pagaremos ao cliente

## Requisitos

Abaixo estão as regras de negócio da implementação que foi feita. Você deve usar isso como base para entender se implementação faz o que deveria da maneira correta.

1. O serviço deve processar transações, recebendo as seguintes informações:
    * Valor da transação
    * Descrição da transação. Ex: `'Smartband XYZ 3.0'`
    * Método de pagamento (`debit_card` ou `credit_card`)
    * Número do cartão
    * Nome do portador do cartão
    * Data de validade do cartão
    * Código de verificação do cartão (CVV)
2. O serviço deve retornar uma lista das transações já criadas
3. Como o número do cartão é uma informação sensível, o serviço só pode armazenar e retornar os 4 últimos dígitos do cartão.
4. O serviço deve criar os recebíveis do cliente (`payables`), com as seguintes regras:
    * Se a transação for feita com um cartão de débito:
    * O payable deve ser criado com status = `paid` (indicando que o cliente já recebeu esse valor)
    * O payable deve ser criado com a data de pagamento (payment_date) = data da criação da transação (D+0).
    * Se a transação for feita com um cartão de crédito:
    * O payable deve ser criado com status = `waiting_funds` (indicando que o cliente vai receber esse dinheiro no futuro)
    * O payable deve ser criado com a data de pagamento (payment_date) = data da criação da transação + 30 dias (D+30).
5. No momento de criação dos payables também deve ser descontado a taxa de processamento (que chamamos de `fee`) do cliente. Ex: se a taxa for 5% e o cliente processar uma transação de R$100,00, ele só receberá R$95,00. Considere as seguintes taxas:
    * 3% para transações feitas com um cartão de débito
    * 5% para transações feitas com um cartão de crédito
6. O serviço deve prover um meio de consulta para que o cliente visualize seu saldo com as seguintes informações:
    * Saldo `available` (disponível): tudo que o cliente já recebeu (payables `paid`)
    * Saldo `waiting_funds` (a receber): tudo que o cliente tem a receber (payables `waiting_funds`)

## Avaliação

1. Quando você terminar o desafio, você precisa **confirmar para a gente no email** ou thread que você estiver trocando com nosso time de recrutamento. Só iremos olhar seus comentários após essa confirmação no email.
    > **Nota:** quando você der o ok no email, você perderá acesso ao desafio. Fazemos isso por uma limitação técnica. Se você quiser manter o que fez de desafio, recomendamos imprimir a página do review como PDF.
1. Vamos te chamar para conversar e discutir sobre o seu code review.
1. Iremos discutir sobre os pontos que você anotou no review e se aprofundar em alguns pontos específicos. 
1. Vamos aproveitar o contexto do desafio para discutir possíveis cenários alternativos, pontos de melhoria que você enxergou, como você faria determinados pontos diferentes, etc.
1. Somos bem cuidadosos com nossos reviews no dia-a-dia e vamos te avaliar com essa barra em mente. Vamos te perguntar se você colocaria esse código em produção.
1. Lembre que isso vai ser uma troca e que deveria ser uma experiência de aprendizado para os dois lados.
1. Boa sorte :)
